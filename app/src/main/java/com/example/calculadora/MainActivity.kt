package com.example.calculadora

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.os.Parcel
import android.os.Parcelable
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity

class MainActivity() : AppCompatActivity() {

    private lateinit var usuario: String
    private lateinit var contraseña: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Obtener los valores de strings.xml
        usuario = resources.getString(R.string.usuario)
        contraseña = resources.getString(R.string.contraseña)

        // Obtener una referencia al EditText
        val inputUser = findViewById<EditText>(R.id.inputUser)
        val inputPass = findViewById<EditText>(R.id.inputPass)

        // Obtener una referencia al botón
        val btnLogin = findViewById<Button>(R.id.btnLogin)
        val btnSalir = findViewById<Button>(R.id.btnSalir)

        // Hacer algo con el botón obtenido
        btnLogin.setOnClickListener {
            // Obtener el contenido del EditText
            val nombreUsuario = inputUser.text.toString()
            val passUsuario = inputPass.text.toString()

            if (nombreUsuario == usuario && passUsuario == contraseña) {
                val bundle = Bundle()
                bundle.putString("Usuario", nombreUsuario)

                Toast.makeText(applicationContext, "Bienvenido", Toast.LENGTH_SHORT).show()
                val intent = Intent(this@MainActivity, CalculadorActivity::class.java)
                startActivity(intent)
            } else {
                Toast.makeText(
                    applicationContext,
                    "Usuario: $usuario, Contraseña: $contraseña",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }

        btnSalir.setOnClickListener {
            val builder = AlertDialog.Builder(this@MainActivity)
            builder.setTitle("Confirmación")
            builder.setMessage("¿Estás seguro de querer salir?")
            builder.setPositiveButton("Sí") { dialog, _ ->
                // Acciones a realizar si se selecciona "Sí"
                finishAffinity() // Cierra todas las actividades y sale de la aplicación
            }
            builder.setNegativeButton("No") { dialog, _ ->
                // Acciones a realizar si se selecciona "No"
                dialog.dismiss() // Cierra el diálogo sin realizar ninguna acción adicional
            }
            val dialog = builder.create()
            dialog.show()
        }
    }
}

